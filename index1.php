<?php
//include "classes/db.php";
include "models/student.php";
include "models/group.php";
$dbh = DB::getInstance();
$dbh = $dbh->dbh;
$sth = $dbh->query('SELECT * FROM students');
$s_list = $sth->fetchAll();
$sth = $dbh->query('SELECT * FROM groups');
$groups = $sth->fetchAll();
$g_list = array();
foreach ($groups as $key => $value) {
	$g_list[$value['id']] = $value['name'];
}

//echo Student::tableName();
$students = Student::query();
foreach ($students as $key => $value) {
	echo $value->id.' '.$value->fio.' '.$value->group_id.'<br>';
}
echo '<br><br>';
$groups = Group::query();
foreach ($groups as $key => $value) {
	echo $value->id.' '.$value->name.'<br>';
}
/*$student = Student::findOne(2);
$student->fio = 'ИВАНОВ Иван Иваныч';
$student->save();
$student = new Student();
$student->fio = 'Щукин';
$student->group_id = 2;
$student->save();*/
/*$student = Student::findOne(7);
$student->delete();*/


?>
<form action="save.php" method="POST">
	<input type="text" name="fio">
	<select name="group_id">
		<?php
			foreach ($g_list as $key => $value) {
				echo '<option value="'.$key.'">'.$value.'</option>';
			}
		?>
	</select>
	<button type="submit">Сохранить</button>
</form>
<table>
	<tr>
		<td>#</td>
		<td>ФИО</td>
		<td>Группа</td>
		<td></td>
		<td></td>
	</tr>
<?php
	$i = 1;
	foreach ($s_list as $key => $value) {
		echo '<tr>';
		echo '<td>'.$i.'</td>';
		echo '<td>'.$value['fio'].'</td>';
		echo '<td>'.$g_list[$value['group_id']].'</td>';
		echo '<td><a href="edit.php?id='.$value['id'].'">Редактировать</a></td>';
		echo '<td><a href="delete.php?id='.$value['id'].'">Удалить</a></td>';
		echo '</tr>';
		$i++;
	}
?>
</table>