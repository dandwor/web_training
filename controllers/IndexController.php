<?php
include_once './classes/controller.php';
include_once './models/student.php';
class IndexController extends Controller{
	
	public function actionIndex(){
		return $this->render('index');
	}

	//protected function render($view_name, array $pararms); 
}