<?php
include_once './classes/model.php';
class Student extends Model{
	protected $primaryKey = 'id';
	public static function tableName(){
		return 'students';
	}
	public static function attributes(){
		return [
			'id',
			'fio',
			'group_id',
		];
	}
}