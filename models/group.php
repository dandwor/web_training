<?php
include_once './classes/model.php';
class Group extends Model{
	protected $primaryKey = 'id';
	public static function tableName(){
		return 'groups';
	}
	public static function attributes(){
		return [
			'id',
			'name',
		];
	}
}