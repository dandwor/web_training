<?php
//'mysql:host=localhost;dbname=05560_web;charset=utf8','root', ''
	return [
		'classes' => [
			'db' => [
				'host' => 'localhost',
				'dbname' => '05560_web',
				'charset' => 'utf8',
				'user' => 'root',
				'pass' => '',
			],
			'router' => [
				'routes' => [
					'stud' => 'student/student',
					'wheel-of-fortune' => 'student/wheel',
				],
			],
		]
	];