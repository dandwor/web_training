<?php
include_once "db.php";
include_once "router.php";
include_once "config.php";
class Sys{
	private static $params = array();

	public static function get($key){
		if(isset(self::$params[$key])){
			return self::$params[$key];
		}
	}

	public static function set($key, $value){
		self::$params[$key] = $value;
	}

	protected static function readConfig(){
		$config = include_once "./config/config.php";
		if(isset($config['classes'])){
			Config::read($config['classes']);
			/*foreach ($config['classes'] as $class => $class_params) {
				echo $class."<br>";
				print_r($class_params);
				$class::setConfig($class_params);
			}*/
		}
		self::set('router', new Router());
	}

	public static function run(){
		self::readConfig();
		//$action = $request;
		//print_r($_REQUEST);
		$route = Router::match();
		$controller = $route['controller'];
		$action = $route['action'];
		self::set('controller', $controller);
		self::set('action', $action);
		include_once "./controllers/".$controller.'.php';
		//echo $controller.' '.$action;
		
		$c = new $controller();
		$reflection = new ReflectionClass(get_class($c));
		$method = $reflection->getMethod($action)->getClosure($c);
		call_user_func_array($method, $route['params']);
		//$c->$action();
	}
}