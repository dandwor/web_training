<?php
include_once "./classes/db.php";
abstract class Model{
	protected $isNew = true;
	protected $primaryKey; 
	private $values;
	public function __construct(){
		$class = static::class;
		$attr = $class::attributes();
		foreach ($attr as $key => $value) {
			$this->values[$value] = '';
		}
		$this->isNew = true;
	}
	public static function tableName(){}
	public static function attributes(){} 
	//$params = [['operator(<,>,...)', 'name', 'value', 'AND'],...] WHERE
	public static function query($params = array()){
		$dbh = DB::getInstance()->dbh;
		$class = static::class;
		//echo $class::tableName();
		$query = 'SELECT * FROM '.$class::tableName();
		if(count($params) != 0){
			$query .= ' WHERE ';
			foreach ($params as $key => $value) {
				$query .= $value[1].' '.$value[0].' '.$value[2].' ';
				if(isset($value[3])){
					$query .= $value[3];
				}
			}
		}
		//print_r($query);
		$res = $dbh->query($query)->fetchAll();
		$models = array();
		$attributes = $class::attributes();
		foreach ($res as $key => $value) {
			$model = new $class();
			foreach ($attributes as $k => $val) {
				$model->$val = $value[$val];
			}
			$model->isNew = false;
			$models[] = $model;
		}
		return $models;
	}

	public static function findOne($id, $key = 'id'){
		$class = static::class;
		$res = $class::query([['=', $key, $id]]);
		if(isset($res[0])){
			return $res[0];	
		}
		return false;
	}

	public function save(){
		$dbh = DB::getInstance()->dbh;
		$class = static::class;
		if($this->isNew){
			//INSERT INTO tabl_name (attr1, attr2) VALUES (1,2)
			$query = 'INSERT INTO '.$class::tableName().'(';
			foreach ($class::attributes() as $key => $value) {
				if($value != $this->primaryKey){
							$query .= $value.', ';
						}
						}			
			$query = substr($query, 0, -2);
			$query .= ') VALUES (';
			foreach ($class::attributes() as $key => $value) {
				if($value != $this->primaryKey){
							$query .= ':'.$value.', ';
						}
						}
			$query = substr($query, 0, -2);
			$query .= ')';
			$sth = $dbh->prepare($query);
			foreach ($class::attributes() as $key => $value) {
				if($value != $this->primaryKey){
					$sth->bindParam(':'.$value, $this->$value);
				}
			}
		}
		else{
			//UPDATE tbl_name SET attr1=val1, attr2=val2 WHERE attr3=val3
			$query = 'UPDATE '.$class::tableName().' SET ';
			foreach ($class::attributes() as $key => $value) {
			 	$query .= $value.'='.':'.$value.', ';
			} 
			$query = substr($query, 0, -2);
			$query .= ' WHERE '.$this->primaryKey.'='.':'.$this->primaryKey;
			$sth = $dbh->prepare($query);
			foreach ($class::attributes() as $key => $value) {
				$sth->bindParam(':'.$value, $this->$value);
			}

		}
		if($sth->execute()){
			return true;
		}
		return false;
		
	}

	public function delete(){
		$dbh = DB::getInstance()->dbh;
		$class = static::class;
		$PK = $this->primaryKey;
		$query = 'DELETE FROM '.$class::tableName().' WHERE '.$PK.'='.':'.$PK;
		$sth = $dbh->prepare($query);
		$sth->bindParam(':'.$PK, $this->$PK);
		if($sth->execute()){
			return true;
		}
		return false;
	} 

	public function __get($key){
		if(isset($this->values[$key])){
			return $this->values[$key];
		}
		return false;
	}

	public function __set($key, $value){
		if($key != $this->primaryKey){
			if(isset($this->values[$key])){
				$this->values[$key] = $value;
			}	
		}
	}
}