<?php
include_once './classes/sys_model.php';
class Router extends Sys_model{

	protected static $action;
	protected static $controller;

	public static function getAction(){
		return self::$action;
	}

	public static function getController(){
		return self::$controller;	
	}

	public static function match(){
		$uri = $_SERVER['REQUEST_URI'];
		//$uri = explode('/', $uri);
		$get_param = explode('?', $uri);
		if(count($get_param) > 1){
			$get_param = $get_param[1];
		}
		else{
			$get_param = '';	
		}
		//print_r($get_param);
		$get_param = explode('&', $get_param);
		$params = array();
		if(count($get_param) !== 0){
			foreach ($get_param as $key => $value) {
				$f = explode('=', $value);
				if(isset($f[0]) && isset($f[1])){
					$params[$f[0]] = $f[1];
				}
			}
		}

		$request = explode('?', $uri);
		$request = explode('/',$request[0]);
		unset($request[0]);
		$request = implode('/', $request);
		if(strlen($request) != 0){
			if($request[strlen($request) - 1] == '/'){
				$request = substr($request, 0, -1);	
			}
		}
		
		//echo $request;
		if(isset(self::$config['routes'][$request])){
			$c_a = explode('/', self::$config['routes'][$request]);
			self::$controller = strtolower($c_a[0]);	
			self::$action = strtolower($c_a[1]);
			$controller = strtolower($c_a[0]);	
			$action = strtolower($c_a[1]);
		}
		else{
			$uri = explode('/', $request);
			//print_r($uri);
			$controller = $uri[0];
			if($controller == ''){
				$controller = 'index';
			}
			if(isset($uri[1])){
				$action = $uri[1];
			}
			else{
				$action = '';
			}
		}
		

		if($action == ''){
			$action = 'index';
		}
		
		self::$action = strtolower($action);
		self::$controller = strtolower($controller);
		
		
		$action[0] = strtoupper($action[0]);
		//echo $action;
		$controller[0] = strtoupper($controller[0]);
		//$controller = explode('?', $controller);
		//$controller = $controller[0];
		//print_r($controller.' '.$action);

		//print_r(self::$config);
		$controller.='Controller';
		$action = 'action'.$action;
		return ['controller' => $controller, 'action' => $action, 'params' => $params];
	}
}