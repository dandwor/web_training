<?php
class Config{

	private static $config;

	public static function read($arr){
		self::$config = $arr;
	}

	public static function get($key){
		//print_r(self::$config);
		if(isset(self::$config[$key])){
			return self::$config[$key];
		}
		return [];
	}
}