<?php
include_once "./classes/sys_model.php";
class DB extends Sys_model{
	private static $instance;
	public $dbh;
	private function __construct(){
		parent::__construct();
		//print_r(self::$config);
		$this->dbh = new PDO('mysql:host='.self::$config['host'].';dbname='.self::$config['dbname'].';charset='.self::$config['charset'],self::$config['user'], self::$config['pass']);
	}

	public static function getInstance(){
		if(!isset(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}
}