<?php
include_once 'system.php';
include_once 'router.php';
abstract class Controller{

	/*protected $post = $_POST;
	protected $get = $_GET;*/

	protected $viewPath = './views/';
	
	public abstract function actionIndex();

	protected function beforeAction(){
		 
	}

	public function action(){

	}

	//$params = ['key' => $value, 'key1' => $value1, 'key2' => $value2, ...]

	protected function render($view, array $params = array()){
		$controller = Router::getController();
		$view_path = $this->viewPath.$controller.'/'.$view.'.php';
		if(is_file($view_path)){
			if(is_array($params) && count(
				$params) > 0){
				ob_start();
				extract($params);
				
				//ob_clean();
			}
			include_once $view_path;
			return true;	
		}
		return false;
	}

	private function checkView(){

	} 
}