<?php
include_once "config.php";
abstract class Sys_model{
	protected static $config;

	/*public static function setConfig($arr){
		self::$config = $arr;
	}*/

	public function __construct(){
		$class = strtolower(static::class);
		//echo $class;
		//print_r(Config::get($class));
		static::$config = Config::get($class);
	}
}