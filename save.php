<?php
include 'db.php';
if(isset($_POST['fio'])&&
	isset($_POST['group_id'])&&
	strlen($_POST['fio'])!=0){
	if(!isset($_POST['id'])){
		$sth = $dbh->prepare('INSERT INTO students(fio, group_id) VALUES (:fio, :group_id)');
		$sth->bindParam(':fio', $_POST['fio']);
		$sth->bindParam(':group_id', $_POST['group_id']);
		$sth->execute();
	}
	else{
		$sth = $dbh->prepare('UPDATE students SET fio = :fio, group_id = :group_id WHERE id=:id');
		$sth->bindParam(':fio', $_POST['fio']);
		$sth->bindParam(':group_id', $_POST['group_id']);
		$sth->bindParam(':id', $_POST['id']);
		$sth->execute();
	}
}
header('Location: index.php');